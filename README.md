Jxls POI
============

The module contains source code for [Jxls POI Transformer](http://jxls.sf.net/reference/main_concepts.html)
and is based on [Apache POI](http://poi.apache.org/)

***
##NOTE

*Starting from v1.1.0 the jxls-poi module was moved to [jxls repo](https://bitbucket.org/leonate/jxls/src/master/).*

*All the further development will be continued in [jxls repo](https://bitbucket.org/leonate/jxls/src/master/).*
